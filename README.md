# Pasos para iniciar un proyecto con node, y generar la misma config de este proyecto de prueba

### Enlaces a la documentacion de estas herramientas

- [BIOME](https://biomejs.dev/)
- [HUSKY](https://typicode.github.io/husky/)

Iniciar un proyecto con npm y node
```bash
npm init -y
```

Instalar las dependencias necesarias
```bash
npm i -ED @biomejs/biome husky
```

Iniciar la config de biome
```bash
npx biome init # Crea el archivo `biome.json`
```

Iniciar la config de husky
```bash
npx husky init # Crea el directorio `.husky` y carga en `scripts` el `prepare` para iniciar el hook
```

```
# Archivo pre-commit dentro del `.husky`

npm run test
```

```
# Archivo pre-push dentro del `.husky`

npm run lint
```

Actualiza el `package.json` en `scripts` los siguientes comandos para validar y arreglar los problemas detectados por biome
```json
{
    "scripts": {
        "lint": "biome check .",
        "lint:fix": "biome check --apply ."
    }
}
```
